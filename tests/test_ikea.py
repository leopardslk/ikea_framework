import allure


@allure.title("Test for navigating through categories,  product choose and check product page title")
def test_get_product(main_page):
    with allure.step("Given: With main page open"):
        with allure.step("When: Choosing category in marketplace"):
            main_page.menu_button_main_page()
            main_page.choose_category_marketplace()

    with allure.step("Then: Choose 'Shop All' category"):
        shop_all = main_page.choose_shop_all_subcategory()

    with allure.step("Then: Choose 'Lighting' subcategory"):
        chosen_category = shop_all.choose_lighting_category()

    with allure.step("Then: Chose product"):
        product = chosen_category.choose_product()

    with allure.step("Then: Check product page title"):
        title = 'LAUTERS Floor lamp, ash/white - IKEA'
        assert product.title_check() == title





