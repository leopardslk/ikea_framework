import allure


@allure.title("Test for checking correct sort price from low to high option for product")
def test_sort_price_low_to_high(main_page):
    with allure.step("Given: With main page open"):
        with allure.step("When: Sending 'Sofa' request to search bar"):
            product_page = main_page.search_request()

    with allure.step("Then: Clicking sort button"):
        product_page.sort_button_click()

    with allure.step("Then: Choose 'Price: low to high' option in sort menu"):
        product_page.click_price_low_to_high_submenu_option()

    with allure.step("Then: Retrieving products prices"):
        prices = product_page.product_price_list()
        print(prices)

    with allure.step("Then: Checking if prices are sorted correctly"):
        for item in prices:
            current_price = prices[item]
            next_price = prices[item + 1]
            assert next_price >= current_price, f"Prices are sorted incorrectly at item {item}"




















