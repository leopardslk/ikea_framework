def test_add_cookie(main_page):
    main_page.cookies.add_cookie("new_cookie", "1")
    new_cookie = main_page.cookies.get_cookie("new_cookie")
    assert new_cookie['value'] == "1"
