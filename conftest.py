
import pytest

from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager

from pages.main_page import MainPage


@pytest.fixture(scope="session")
def driver():
    driver = webdriver.Chrome(ChromeDriverManager().install())

    # serv_obj = Service("//chromedriver.exe")
    # driver = webdriver.Chrome(service=serv_obj)

    driver.get("https://www.ikea.com/us/en/")
    driver.maximize_window()
    yield driver

    driver.quit()


@pytest.fixture(scope="session")
def main_page(driver):
    yield MainPage(driver)


