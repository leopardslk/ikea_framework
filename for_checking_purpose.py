import re
import time

from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager


driver = webdriver.Chrome(ChromeDriverManager().install())
driver.get('https://www.ikea.com/us/en/search/?q=sofa')

driver.maximize_window()
time.sleep(1)
driver.find_element(By.XPATH, '//button[@aria-label="Show sorting options modal"]').click()
driver.find_element(By.XPATH, '//input[@id="plp-PRICE_LOW_TO_HIGH"]').click()
time.sleep(1)
driver.find_element(By.XPATH, '//button[@aria-label="Show sorting options modal"]').click()
time.sleep(1)
product_list = driver.find_elements(By.XPATH, '//span[@class="plp-price__sr-text"]')


print("products:", product_list)
time.sleep(1)
prices = [element.text for element in product_list]
print("products_prices:", prices)

# Regular expression pattern to match float format
pattern = re.compile(r'\d+\.\d+')

# Extract float numbers from the prices list
float_prices = []
for price in prices:
    matches = re.findall(pattern, price)
    for match in matches:
        float_prices.append(float(match))

print('Prices in float:', float_prices)






