from typing import Tuple
from selenium.webdriver import Chrome, Keys
from selenium.webdriver.common.by import By
import allure

from pages.base_page import BasePage
from pages.product import Product
from pages.shop_all import ShopAll


class MainPage(BasePage):
    _menu_button_locator:  Tuple[By, str] = (By.XPATH, "//button[@class='hnf-btn hnf-btn--small hnf-btn--icon-tertiary hnf-btn--open-menu']")
    _marketplace_category_locator:  Tuple[By, str] = (By.XPATH, "//nav[@class='hnf-menu__nav']//a[text()='Home Accessories']")
    _shop_all_subcategory_locator:  Tuple[By, str] = (By.XPATH, '//ul[@aria-label="Submenu for Home Accessories"]//a[text()="Shop all"]')
    _search_bar_locator: Tuple[By, str] = (By.XPATH, '//input[@id="ikea-search-input"]')
    _search_request: str = "sofa"

    def __init__(self, driver: Chrome):
        super().__init__(driver)

    @allure.step("Click meny button")
    def menu_button_main_page(self):
        self.click(self._menu_button_locator)

    @allure.step("Choose marketplace category ")
    def choose_category_marketplace(self):
        self.click_for_element_to_be_clickable(self._marketplace_category_locator)

    @allure.step("Choose 'Shop all' subcategory")
    def choose_shop_all_subcategory(self):
        self.click(self._shop_all_subcategory_locator)
        return ShopAll(self.driver)

    @allure.step("Send search request")
    def search_request(self):
        search_bar = self.waiting_for_element(self._search_bar_locator)
        search_bar.send_keys(self._search_request)
        search_bar.send_keys(Keys.RETURN)

        return Product(self.driver)

