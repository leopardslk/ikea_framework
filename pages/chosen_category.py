from typing import Tuple

import allure
from selenium.webdriver.common.by import By

from pages.base_page import BasePage
from pages.product import Product


class ChosenCategory(BasePage):
    _chosen_product_locator: Tuple[By, str] = (By.XPATH, '//div[@id="pub__carousel__e27d5760-8333-11ee-8325-9d93379116a7"]/child::div[1]')

    def __init__(self, driver):
        super().__init__(driver)

    @allure.step("Choose product, click product")
    def choose_product(self):
        self.click(self._chosen_product_locator)
        return Product(self.driver)


