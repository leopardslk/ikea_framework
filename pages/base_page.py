from typing import Tuple

import allure
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from selenium.webdriver import Chrome

from cookies.cookies import Cookies


class BasePage:
    def __init__(self, driver: Chrome):
        self.driver = driver
        self.wait = WebDriverWait(self.driver, 10)
        self.cookies = Cookies(driver)

    @allure.step("Waiting for presence of element")
    def waiting_for_element(self, locator: Tuple[By.XPATH, str]):
        element: WebElement = self.wait.until(EC.presence_of_element_located(locator))
        return element

    @allure.step("Click element")
    def click(self, locator: Tuple[By.XPATH, str]):
        element: WebElement = self.waiting_for_element(locator)
        element.click()

    @allure.step("Waiting for element to be clickable")
    def waiting_for_element_to_be_clickable(self, locator: Tuple[By.XPATH, str]):
        element: WebElement = self.wait.until(EC.element_to_be_clickable(locator))
        return element

    @allure.step("Click for element to be clickable")
    def click_for_element_to_be_clickable(self, locator: Tuple[By.XPATH, str]):
        element: WebElement = self.waiting_for_element_to_be_clickable(locator)
        element.click()

