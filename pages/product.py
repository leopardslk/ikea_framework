import re
from typing import Tuple

import allure
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement

from pages.base_page import BasePage


class Product(BasePage):
    _product_title_locator: Tuple[By, str] = (By.XPATH, "//title[text()='LAUTERS Floor lamp, ash/white - IKEA']")
    _sort_button_locator: Tuple[By, str] = (By.XPATH, '//button[@aria-label="Show sorting options modal"]')
    _price_low_to_high_radio_button_locator: Tuple[By.XPATH, str] = (By.XPATH, '//input[@id="plp-PRICE_LOW_TO_HIGH"]')
    _products_prices: Tuple[By.XPATH, str] = (By.XPATH, '//span[@class="plp-price__sr-text"]')

    def __init__(self, driver):
        super().__init__(driver)

    @allure.step("Check product page title")
    def title_check(self):
        self.waiting_for_element(self._product_title_locator)
        return self.driver.title

    @allure.step("Click 'Sort' button")
    def sort_button_click(self):
        self.click_for_element_to_be_clickable(self._sort_button_locator)

    @allure.step("Choose 'Price: low to high' option")
    def click_price_low_to_high_submenu_option(self):
        self.click(self._price_low_to_high_radio_button_locator)
        self.click(self._sort_button_locator)

    @allure.step("Retrieving products prices")
    def product_price_list(self):
        self.waiting_for_element(self._products_prices)
        price_list = self.driver.find_elements(*self._products_prices)
        prices = [element.text for element in price_list]

        pattern = re.compile(r'\d+\.\d+')
        prices_in_float = []
        for price in prices:
            matches = re.findall(pattern, price)
            for match in matches:
                prices_in_float.append(float(match))

        return prices_in_float









