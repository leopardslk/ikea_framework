from typing import Tuple

import allure
from selenium.webdriver.common.by import By

from pages.base_page import BasePage
from pages.chosen_category import ChosenCategory


class ShopAll(BasePage):
    _lighting_category_locator: Tuple[By, str] = (By.XPATH, '//div[@id="grid-item-marketplace-lighting"]')

    def __init__(self, driver):
        super().__init__(driver)

    @allure.step("Choose category 'Lighting'")
    def choose_lighting_category(self):
        self.click(self._lighting_category_locator)
        return ChosenCategory(self.driver)
