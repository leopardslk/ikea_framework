
from selenium.webdriver import Chrome


class Cookies:
    def __init__(self, driver: Chrome):
        self.driver = driver

    def add_cookie(self, name: str, value: str):
        self.driver.add_cookie({'name': name, 'value': value})

    def get_cookie(self, name: str) -> dict:
        return self.driver.get_cookie(name)
